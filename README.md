# BackLog

## Feature

### Feature 1 观看rtsp录播课程

* 观看录播rtsp视频：
作为用户，可以在网页上观看选中的视频，用来进行课程的学习。
1. 通过vlc客户端，用户可以观看rtsp视频流的播放
2. 三个客户端可以同时观看一个视频

* 查找rtsp课程视频流
作为网站开发人员，可以找到视频编号所对应的视频流地址，用来进行视频播放准备。
1. 通过http协议，提供获取视频流地址，将视频编号post到视频网关
2. 如果是同一个课程，在一定的客户端数量限制内，返回相同的视频流地址
3. 如果所有视频服务器都超出请求限制，通知请求端无可用资源

* (B)设置视频服务器客户端数量限制
作为系统管理人员，可以设置视频服务器最大连接数量，用来保护系统能正常提供服务。
1. 默认值为100
2. 当请求数量超过最大数量后，分配到新的视频服务服务

* (B) 资源报警
作为运帷人员，当视频服务器资源超出限制时，可以收到系统资源报警，用来预防系统突发的请求数量过多。
1. 默认值为80%
2. 发送邮件到指定邮箱

* (B) 添加视频服务器
作为运帷人员，可以添加视频服务器到服务器池中，用来扩充系统资源
1. 配置成功后，可以在服务器资源池中看到新加资源
2. 选择激活后，会有请求派发到此服务器

* (B) 移除视频服务器
作为运帷人员，可以从服务器池中，移除视频服务器，用来进行维护及其他操作
1. 可以查看视频服务器地址及名称
2. 可以选择移除，将不会有新的请求派发到此服务器，状态为移除中
3. 完成后，状态为已删除

* 停止rtsp视频服务
当没有请求时，系统可以自动停止视频服务.
1. 对视频服务没有请求时，可以自动停止，释放系统资源.
2. 当新请求进入时，可以重新进行加载.


### Feature 2 观看直播课程

* 查找rtsp直播视频流
作为网站开发人员，可以发起直播视频请求，用来给客户提供直播视频观看
1. 通过http协议，请求直播视频地址，将视频设备地址post到视频网关
2. 参考r-2/r-1验证

* r－8.1 存储直播视频

### Feature 3 上传视频

* 手工上传视频到服务器
作为网站开发人员，可以上传课程视频资料到视频服务器，用来为客户进行播放。
1. 通过http协议，上传视频文件，成功后，视频路由返回相应的视频编号
2. 视频存储在hdfs中
3. 可以通过在网关中请求编号进行视频播放

* 自动上传视频到服务器
作为网站开发人员，可以提供ftp地址到视频路由，视频路由自动到ftp服务上获取视频，并进行存储，用来为客户进行播放准备。
1. 通过http协议，请求存储视频，post ftp地址到视频路由
2. 获取到目录及子目录下的所有视频，并保存到hdfs

* 查看自动上传视频
作为网站开发人员，可以查看通过自动上传获取到的视频，用来准备课程信息。
1. 通过http协议，查看到所有自动上传的视频编号，可以选择进行播放
2. 可以查看到 日期，视频编号，ftp地址，绑定状态

* 绑定／取消绑定自动上传视频
作为网站开发人员，可以绑定课程到自动上传的视频，用来进行课程绑定。
1. 通过http协议，绑定／取消绑定课程


### Feature 4 视频处理

* 视频码流转换
作为网站开发人员，可以选择将低码流视频提供给客户，用来方便移动端的客户进行观看
1. 通过http协议，请求视频时，可以提供低码流参数，视频网关将返回低码流视频地址

* 视频截图
作为网站开发人员，可以选择视频截图，用来查看视频服务的内容，并提供给客户进行初步的视频内容了解
1. 公共http协议，请求视频截图，返回图片


## 接口

1. 获取视频

    POST http://gateway-n/gateway/v1/play/record/xxx-xxx-xxx-xxx?vf=low

    其中gateway-n为视频网关地址，xxx-xxx-xxx-xxx为视频编号，vf为码流参数(可选)

    返回示例 {address: "rtsp://media-server-n/a/b/c"}

2. 直播视频

    POST http://gateway-n/gateway/v1/play/stream?addr=rtsp://direct-server/a/b&vf=low

    其中gateway-n为视频网关地址，address=rtsp://direct-server/a/b参数为直播地址，vf为码流参数(可选)

    返回示例 {address: "rtsp://media-server-n/d/e"}

3. 上传视频

    POST http://route-n/route/v1/upload/stream

    multip-part file stream

    返回

    ```json
    {id:"xxx-xxx-xxx"}
    ```

4. 自动上传视频

    POST http://route-n/route/v1/upload/ftp?addr=ftp://machine.x/2014/10

5. 查看上传视频列表

    GET http://route-n/route/v1/list?status=xx&date=2014-09-01&addr=ftp://a.b/xx

    返回示例

    ```json
    [{"id":"xxx","name":"SimpleBallDemo.swf", "path":"ftp://121.40.122.84:2121/sample/sample/2014/08/05/samples","status":"UNBIND","createDate":1408847038000},{}]
    ```

6. 绑定／取消绑定

    POST

    http://route-n/route/v1/bind/xxxx-xxxx-xxxxx

    http://route-n/route/v1/unbind/xxxx-xxxx-xxxxx

6.1 批量绑定／取消绑定

    POST
    http://route-n/route/v1/bind
    ```json
    ['xxx','xxx','xxx']
    ```

6.2 获取单个ftp上传的视频
    GET
    http://route-n/route/v1/media/xxx-xxx-xxx

7. 视频截图

    GET http://route-n/route/v1/capture?v=xxxx-xxx-xxxx&t=1234s

    返回 jpeg／png stream

8. 多路视频合并截图
    GET http://gateway-n/route/v1/capture?v=aaa,bbb,ccc,dddd,eee,fff,ggg,hhh,iii,jjj&t=1234s

    返回 jpeg/png stream

    12
    3
    45

9. 定时录制任务注册
    POST http://route-n/route/v1/recording/all
    [{"startDate":1408866138958,"endDate":1408866138958,"address":["rtmp://a.b.cn/a/b","rtmp://a.b.cn/a/c","rtmp://a.b.cn/a/d"]},{"startDate":1408866138958,"endDate":1408866138958,"address":["rtmp://b.a.cn/a/b"]}]

10. 手工录制
    POST http://route-n/route/v1/recording/one
    {"startDate":1408866138958,"endDate":1408866138958,"address":["rtmp://a.b.cn/a/b","rtmp://a.b.cn/a/c","rtmp://a.b.cn/a/d"]}


## 部署及使用

### deploy router/gateway
 * 增加-Dred5.config.home=/xx/yy, 其中路径中包括gateway.propterties文件，并配置正确
 * 在gateway.properties中配置正确的mysql链接地址
 * 在gateway.properties中配置正确的store.root地址，当前为ftp的根路径
 * 通过 curl -X POST http://a.b.c/nils/task/v1/scanServerStatus 用来移除视频服务器

### deploy ftp server
 * 执行脚本 wget http://mirrors.cnnic.cn/apache/mina/ftpserver/1.0.6/dist/ftpserver-1.0.6.tar.gz
 * tar -xvzf ftpserver-1.0.6.tar.gz
 * bin/ftpd.sh res/conf/demo.xml
 * 通过配置demo.xml来配置端口及跟路径

### deploy alice
 * curl -X POST http://a.b.c:9001/tasks/register 进行服务注册
 * java -jar alice.jar server alice.yml 其中alice.yml为alice服务的配置
{"name":1408806829704,"key":1408834824704,"path":"rtmp://1.2.3/4/5"}

## 示例

* ftp upload
 POST http://121.40.122.84:8080/nils/route/v1/upload/ftp?addr=ftp://121.40.122.84:2121/sample

* list all
 GET http://121.40.122.84:8080/nils/route/v1/list

* bind
 POST http://121.40.122.84:8080/nils/route/v1/bind/9c8313ce-783f-4abb-9ab0-dd3bd6abb185

* unbind
 POST http://121.40.122.84:8080/nils/route/v1/unbind/9c8313ce-783f-4abb-9ab0-dd3bd6abb185

* get one media
 GET http://121.40.122.84:8080/nils/route/v1/media/1fe1387a-5bcd-403c-9a7b-bef3ade8f27c

* list bind
 GET http://121.40.122.84:8080/nils/route/v1/list?status=BIND

* list unbind
 GET http://121.40.122.84:8080/nils/route/v1/list?status=UNBIND

* list unbind & date is today
 GET http://121.40.122.84:8080/nils/route/v1/list?status=UNBIND&date=2014-08-24

* capture
 GET http://121.40.122.84:8080/nils/route/v1/capture?v=9c8313ce-783f-4abb-9ab0-dd3bd6abb185&t=1234s

* capture default time
 GET http://121.40.122.84:8080/nils/route/v1/capture?v=9c8313ce-783f-4abb-9ab0-dd3bd6abb185

* live stream
 POST http://121.40.122.84:8080/nils/gateway/v1/play/stream?addr=rtsp://a.b.c/a/b&vf=low

* live stream default
 POST http://121.40.122.84:8080/nils/gateway/v1/play/stream?addr=rtsp://a.b.c/a/b

 return:
 ```json
 {
     "address": "rtmp://127.0.0.1:8080/x"
 }
 ```

* record
 POST http://121.40.122.84:8080/nils/gateway/v1/play/record/9c8313ce-783f-4abb-9ab0-dd3bd6abb185?vf=low

 ```json
 {
     "address": "RTSP://121.41.128.123:49000/2fb048f7-6114-4ebb-a385-87d4bc3dda1e.fla"
 }
 ```


## ffmpeg
* 截取视频文件从某个时间开始的几秒钟，进行简短介绍
```bash
ffmpeg -ss 00:05:00 -i 1.mp4 -t 5 -c copy -map 0 slice.mp4
```

* extract just a single frame from the video into an image file
```bash
ffmpeg -i 1.mp4 -ss 00:05:00 -f image2 -vframes 1 out.png
```

## Test

curl -X POST http://localhost:9001/tasks/register

curl -X POST http://localhost:8080/nils/route/v1/upload/stream -F "file=@/Users/xuhaixiang/workspace/red5/media/1.mp4"

curl -X POST http://192.168.1.220:6080/nils/route/v1/bind/d32d3bc7-9972-4db9-930e-b1a78736745f

