package gv.r5.gateway.services;

import gv.r5.gateway.AbstractServiceTestSuite;
import gv.r5.gateway.domain.LiveMedia;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.Playing;
import gv.r5.gateway.maker.TestData;
import gv.r5.gateway.remote.AliceAdaptor;
import gv.r5.gateway.remote.RemoteProxy;
import gv.r5.gateway.remote.SimpleResult;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import retrofit.http.Body;
import retrofit.http.Query;

import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class PlayServiceTest extends AbstractServiceTestSuite {

    @Mock
    private RemoteProxy client;

    @Autowired
    @InjectMocks
    private PlayService psvc;

    @Autowired
    private TestData data;

    @Before
    public void initMock() {
        MockitoAnnotations.initMocks(this);
        mock_AliceAdaptor_return_success();
    }

    @Test
    public void playRecordSucc() {
        data.makeMediaAndServer(1, 1);
        Playing p = psvc.start("1000", "low");
        assertThat(p, notNullValue());
    }

    private void mock_AliceAdaptor_return_success() {
        when(client.get(eq(AliceAdaptor.class), anyString())).thenReturn(new MyAliceAdaptor());
    }

    @Test(expected = IllegalArgumentException.class)
    public void playRecordNotFound() {
        data.makeMediaAndServer(1, 1);
        Playing p = psvc.start("32768", "low");
    }

    @Test(expected = IllegalArgumentException.class)
    public void playServerNotFound() {
        data.makeMediaAndServer(1, 0);
        psvc.start("1000", "low");
    }

    @Test
    public void playRecordSuccWithThreeServer() {
        data.makeMediaAndServer(2, 3);
        Playing p = psvc.start("1000", "origin");
        assertThat(p, notNullValue());
        Playing pa = psvc.start("1001", "high");
        assertThat(pa, notNullValue());
    }

    @Test
    public void playSameRecordSuccWithThreeServer() {
        data.makeMediaAndServer(2, 3);
        Playing p = psvc.start("1000", "low");
        assertThat(p, notNullValue());
        Playing pa = psvc.start("1000", "low");
        assertThat(pa, notNullValue());
    }


    private class MyAliceAdaptor implements AliceAdaptor {
        @Override
        public SimpleResult start(@Body Media media, @Query("vf") String frame) {
            SimpleResult result = new SimpleResult();
            result.setStatus("SUCCESS");
            result.setStreamUrl("rtsp://a/b.ts");
            return result;
        }

        @Override
        public SimpleResult live(@Body LiveMedia media, @Query("t") Long duration, @Query("f") String frame) {
            return null;
        }

        @Override
        public SimpleResult capture(@Body Media media, @Query("t") Long captureTime) {
            return null;
        }

        @Override
        public SimpleResult capture(@Body LiveMedia media, @Query("t") Long duration) {
            return null;
        }

        @Override
        public SimpleResult recording(@Body Media media, @Query("s") String streamUrl, @Query("dt") Long duration) {
            return null;
        }


    }
}
