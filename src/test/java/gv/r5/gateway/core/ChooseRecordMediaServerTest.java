package gv.r5.gateway.core;

import gv.r5.gateway.AbstractServiceTestSuite;
import gv.r5.gateway.domain.BroadCast;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.maker.TestData;
import gv.r5.gateway.repo.MediaRepo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Created by xuhaixiang on 7/27/14.
 */
public class ChooseRecordMediaServerTest extends AbstractServiceTestSuite {

    @Autowired
    private TestData data;

    @Autowired
    private Evaluator c ;

    @Autowired
    private MediaRepo mediaRepo;


    @Test(expected = IllegalArgumentException.class)
    public void one_media_in_live_server_throw_error(){
        data.makeMediaAndServer(1, 1, BroadCast.LIVE);
        c.recorded(mediaRepo.get(1L));
    }

    @Test
    public void one_media_in_three_live_server_one_record_server(){
        data.makeMediaAndServer(1, 3, BroadCast.LIVE);
        data.makeMediaAndServer(1, 1, BroadCast.RECORDED);
        MediaServer recorded = c.recorded(mediaRepo.get(1L));
        assertThat(recorded.getBroadCast(), is(BroadCast.RECORDED));
    }

    @Test
    public void two_media_in_three_live_server_one_record_server(){
        data.makeMediaAndServer(1, 3, BroadCast.LIVE);
        data.makeMediaAndServer(1, 1, BroadCast.RECORDED);
        MediaServer recorded = c.recorded(mediaRepo.list().get(0));
        MediaServer recorded2 = c.recorded(mediaRepo.list().get(1));
        assertThat(recorded.getBroadCast(), is(BroadCast.RECORDED));
        assertThat(recorded.getId(), is(recorded2.getId()));
    }

    @Test
    public void two_media_in_three_live_server_one_both_server(){
        data.makeMediaAndServer(1, 3, BroadCast.BOTH);
        data.makeMediaAndServer(1, 1, BroadCast.RECORDED);
        MediaServer recorded = c.recorded(mediaRepo.list().get(0));
        MediaServer recorded2 = c.recorded(mediaRepo.list().get(1));
        assertThat(recorded, notNullValue());
        assertThat(recorded2, notNullValue());
    }

}
