package gv.r5.gateway.web.controller.router;

import gv.r5.gateway.AbstractControllerTestSuite;
import gv.r5.gateway.services.CaptureService;
import gv.r5.gateway.web.controller.RouteController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CaptureRouteTestControllerTest extends AbstractControllerTestSuite {

    @Autowired
    @InjectMocks
    RouteController routeController;

    @Mock
    CaptureService captureService;

    @Before
    public void initMock() throws IOException {
        MockitoAnnotations.initMocks(this);
        when(captureService.capture(anyString(), anyLong(), any(TimeUnit.class))).thenReturn(new byte[0]);
    }

    @Test
    public void capture() throws Exception {
        mockMvc.perform(get(routePrefix + "/capture?v=123-322&t=222"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.IMAGE_PNG))
                .andExpect(content().bytes(new byte[0]));
    }
}
