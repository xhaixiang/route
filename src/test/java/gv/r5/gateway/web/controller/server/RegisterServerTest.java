package gv.r5.gateway.web.controller.server;

import gv.r5.gateway.AbstractControllerTestSuite;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.persistence.MediaServerMapper;
import gv.r5.gateway.repo.MediaServerRepo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RegisterServerTest extends AbstractControllerTestSuite {

    @Autowired
    private MediaServerMapper mapper;

    @Test
    public void registerSuccess() throws Exception {
        mockMvc.perform(post(serverPrefix + "/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"a\", \"host\":\"192.168.2.1\", \"port\":4192, \"size\":100}"))
                .andExpect(status().isOk());
    }

    @Test
    public void registerDuplicate() throws Exception {
        mockMvc.perform(post(serverPrefix + "/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"b\", \"host\":\"192.168.2.1\", \"port\":4192, \"size\":100}"))
                .andExpect(status().isOk());
        MediaServer b1 = mapper.getActivedServer("b");
        mockMvc.perform(post(serverPrefix + "/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"b\", \"host\":\"192.168.2.1\", \"port\":4192, \"size\":100}"))
                .andExpect(status().isOk());
        MediaServer b2 = mapper.getActivedServer("b");
        assertNotNull(b2.getUpdateDate().after(b1.getUpdateDate()));
    }

    @Test
    public void startingSucc() throws Exception {
        mockMvc.perform(post(serverPrefix + "/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"b\", \"host\":\"192.168.2.1\", \"port\":4192, \"size\":100}"))
                .andExpect(status().isOk());
        MediaServer b = mapper.getActivedServer("b");
        mapper.suspend(b.getId());
        mockMvc.perform(post(serverPrefix + "/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"b\", \"host\":\"192.168.2.1\", \"port\":4192, \"size\":100}"))
                .andExpect(status().isOk());
        assertNotNull(mapper.getActivedServer("b"));
    }

}
