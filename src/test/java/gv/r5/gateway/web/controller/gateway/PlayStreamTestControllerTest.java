package gv.r5.gateway.web.controller.gateway;

import gv.r5.gateway.AbstractControllerTestSuite;
import gv.r5.gateway.domain.Playing;
import gv.r5.gateway.services.PlayService;
import gv.r5.gateway.web.controller.GatewayController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PlayStreamTestControllerTest extends AbstractControllerTestSuite {

    @Autowired
    @InjectMocks
    GatewayController gatewayController;

    @Mock
    PlayService playService;

    @Before
    public void initMock() {
        MockitoAnnotations.initMocks(this);
        Playing p = new Playing();
        p.setAddress("rtmp://a.b");
        when(playService.live("rtsp://a.b.c/123", 3600L, null)).thenReturn(p);
        Playing plow = new Playing();
        plow.setAddress("rtmp://a.c");
        when(playService.live("rtsp://a.b.c/123", 3600L, "low")).thenReturn(plow);
    }

    @Test
    public void playStream_with_Standard_Frame() throws Exception {
        mockMvc.perform(post(gatewayPrefix+"/play/stream?addr=rtsp://a.b.c/123")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("{\"address\":\"rtmp://a.b\"}"));
    }

    @Test
    public void playSteam_with_different_stream() throws Exception {
        mockMvc.perform(post(gatewayPrefix+"/play/stream?addr=rtsp://a.b.c/123&vf=low")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("{\"address\":\"rtmp://a.c\"}"));
    }
}
