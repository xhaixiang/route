package gv.r5.gateway.web.controller.gateway;

import gv.r5.gateway.AbstractControllerTestSuite;
import gv.r5.gateway.domain.LiveMedia;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.maker.TestData;
import gv.r5.gateway.remote.AliceAdaptor;
import gv.r5.gateway.remote.RemoteProxy;
import gv.r5.gateway.remote.SimpleResult;
import gv.r5.gateway.services.PlayService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import retrofit.http.Body;
import retrofit.http.Query;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class PlayRecordTestControllerTest extends AbstractControllerTestSuite {

    @Autowired
    private TestData data;

    @Mock
    private RemoteProxy client;

    @Autowired
    @InjectMocks
    private PlayService playService;

    @Before
    public void initMock() {
        MockitoAnnotations.initMocks(this);
        mock_AliceAdaptor_return_success();
    }


    private void mock_AliceAdaptor_return_success() {
        when(client.get(eq(AliceAdaptor.class), anyString())).thenReturn(new MyAliceAdaptor());
    }

    @Test
    public void playRecord_With_Standand_Frame() throws Exception {
        data.makeMediaAndServer(1, 1);
        mockMvc.perform(post(gatewayPrefix + "/play/record/1000")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("{\"address\":\"rtsp://127.0.0.0:49000/a0\"}"));
    }

    @Test
    public void playRecord_With_Differen_Frame() throws Exception {
        data.makeMediaAndServer(1, 1);
        mockMvc.perform(post(gatewayPrefix + "/play/record/1000?vf=low")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("{\"address\":\"rtsp://127.0.0.0:49000/a0\"}"));
    }

    private class MyAliceAdaptor implements AliceAdaptor {
        @Override
        public SimpleResult start(@Body Media media, @Query("vf") String frame) {
            SimpleResult result = new SimpleResult();
            result.setStatus("SUCCESS");
            result.setStreamUrl("rtsp://127.0.0.0:49000/a0");
            return result;
        }

        @Override
        public SimpleResult live(@Body LiveMedia media, @Query("t") Long duration, @Query("f") String frame) {
            return null;
        }

        @Override
        public SimpleResult capture(@Body Media media, @Query("t") Long captureTime) {
            return null;
        }

        @Override
        public SimpleResult capture(@Body LiveMedia media, @Query("t") Long duration) {
            return null;
        }


        @Override
        public SimpleResult recording(@Body Media media, @Query("s") String streamUrl, @Query("dt") Long duration) {
            return null;
        }


    }
}