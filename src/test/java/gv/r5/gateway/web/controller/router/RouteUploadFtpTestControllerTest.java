package gv.r5.gateway.web.controller.router;

import gv.r5.gateway.AbstractControllerTestSuite;
import gv.r5.gateway.services.FtpService;
import gv.r5.gateway.web.controller.RouteController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RouteUploadFtpTestControllerTest extends AbstractControllerTestSuite {

    @Mock
    FtpService ftpService;

    @Autowired
    @InjectMocks
    RouteController routeController;

    @Before
    public void initMock() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void upload_with_ftp() throws Exception {
        mockMvc.perform(post(routePrefix + "/upload/ftp?addr=ftp://a.b.c/2014/03")).andExpect(status().isOk());
    }
}
