package gv.r5.gateway.web.controller;

import gv.r5.gateway.AbstractControllerTestSuite;
import org.junit.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class IndexTestControllerTest extends AbstractControllerTestSuite {

    @Test
    public void home() throws Exception {
        mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(view().name("home/index"));
    }

    @Test
    public void simple() throws Exception {
        mockMvc.perform(get("/simple")).andExpect(status().isOk()).andExpect(content().string("simple reponse"));
    }

}