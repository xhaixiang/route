package gv.r5.gateway.web.controller.router;

import gv.r5.gateway.AbstractControllerTestSuite;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

public class RouteUploadManualTestControllerTest extends AbstractControllerTestSuite {

    @Test
    public void uploadFileSucc() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "orig", null, "bar".getBytes());
        mockMvc.perform(fileUpload(routePrefix + "/upload/stream").file(file))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }
}
