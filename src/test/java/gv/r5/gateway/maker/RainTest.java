package gv.r5.gateway.maker;

import com.natpryce.makeiteasy.Maker;
import gv.r5.gateway.domain.LifeCycle;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.domain.Playing;
import org.hamcrest.core.IsEqual;
import org.junit.Test;

import static com.natpryce.makeiteasy.MakeItEasy.*;
import static gv.r5.gateway.maker.RainMaker.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.*;

public class RainTest {

    @Test
    public void produce_media() {
        Maker<Media> mm = a(MEDIA);
        Media mediaA = make(mm);
        Media mediaB = make(mm);
        assertNull(mediaA.getId());
        assertNotNull(mediaA.getName());
        assertNotNull(mediaA.getKey());
        assertNotNull(mediaA.getPath());
        assertThat(mediaA.getName(), not(equalTo(mediaB.getName())));

        Media mediaC = make(mm.but(with(RainMaker.mediaLC, LifeCycle.UNINSTALLED)));
        assertThat(mediaC.getStatus(), IsEqual.equalTo(LifeCycle.UNINSTALLED));

    }

    @Test
    public void produce_media_server() {
        Maker<MediaServer> ms = a(MEDIA_SERVER);
        MediaServer msA = make(ms);
        MediaServer msB = make(ms);

        assertThat(msA.getId(), nullValue());
        assertThat(msA.getName(), not(equalTo(msB.getName())));
        assertThat(msA.getHost(), equalTo(msB.getHost()));
    }

    @Test
    public void produce_playing() {
        Maker<Playing> mp = a(PLAYING);
        Playing pA = make(mp);
        Playing pB = make(mp);
        assertThat(pA.getId(), nullValue());
        assertThat(pA.getServerId(), not(equalTo(pB.getServerId())));
        assertThat(pA.getMediaId(), not(equalTo(pB.getMediaId())));
    }


}
