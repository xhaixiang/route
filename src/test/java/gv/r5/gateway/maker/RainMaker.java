package gv.r5.gateway.maker;

import com.natpryce.makeiteasy.Instantiator;
import com.natpryce.makeiteasy.Property;
import com.natpryce.makeiteasy.PropertyLookup;
import gv.r5.gateway.domain.*;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.UUID;

import static com.natpryce.makeiteasy.Property.newProperty;

public class RainMaker {
    public static final Property<Media, String> mediaKey = newProperty();
    public static final Property<Media, String> mediaName = newProperty();
    public static final Property<Media, String> mediaPath = newProperty();
    public static final Property<Media, LifeCycle> mediaLC = newProperty();

    public static final Property<MediaServer, String> serverName = newProperty();
    public static final Property<MediaServer, String> serverHost = newProperty();
    public static final Property<MediaServer, Integer> serverPort = newProperty();
    public static final Property<MediaServer, Integer> serverLimit = newProperty();
    public static final Property<MediaServer, BroadCast> serverType = new Property<>();
    public static final Property<MediaServer, MediaProtocol> serverProtocol = new Property<>();

    public static final Property<Playing, Long> playServer = newProperty();
    public static final Property<Playing, Long> playMedia = newProperty();
    public static final Instantiator<Media> MEDIA = new Instantiator<Media>() {

        @Override
        public Media instantiate(PropertyLookup<Media> lookup) {
            Media m = new Media(lookup.valueOf(mediaKey, UUID.randomUUID().toString()),
                    lookup.valueOf(mediaPath, RandomStringUtils.randomAscii(6)));
            m.setName(lookup.valueOf(mediaName, RandomStringUtils.randomAlphabetic(6)));
            m.setStatus(lookup.valueOf(mediaLC, LifeCycle.ACTIVE));
            return m;
        }
    };

    public static final Instantiator<MediaServer> MEDIA_SERVER = new Instantiator<MediaServer>() {
        @Override
        public MediaServer instantiate(PropertyLookup<MediaServer> lookup) {
            MediaServer s = new MediaServer(lookup.valueOf(serverName, RandomStringUtils.randomAscii(6)),
                    lookup.valueOf(serverHost, "127.0.0.1"),
                    lookup.valueOf(serverPort, 49002),
                    lookup.valueOf(serverLimit, 100));
            s.setBroadCast(lookup.valueOf(serverType, BroadCast.RECORDED));
            s.setProtocol(lookup.valueOf(serverProtocol, MediaProtocol.RTSP));
            return s;
        }
    };

    public static final Instantiator<Playing> PLAYING = new Instantiator<Playing>() {
        @Override
        public Playing instantiate(PropertyLookup<Playing> lookup) {
            Playing p = new Playing(lookup.valueOf(playServer, randomLong()),
                    lookup.valueOf(playMedia, randomLong()));
            return p;
        }

        private Long randomLong() {
            return Math.round(Math.random() * 1000);
        }
    };
}
