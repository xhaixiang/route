package gv.r5.gateway.maker;

import gv.r5.gateway.domain.BroadCast;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaProtocol;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.repo.MediaRepo;
import gv.r5.gateway.repo.MediaServerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.natpryce.makeiteasy.MakeItEasy.*;
import static gv.r5.gateway.domain.BroadCast.RECORDED;
import static gv.r5.gateway.domain.MediaProtocol.RTSP;
import static gv.r5.gateway.maker.RainMaker.*;

@Component
public class TestData {
    @Autowired
    private MediaRepo mediaRepo;

    @Autowired
    private MediaServerRepo serverRepo;

    int mkey = 1000;
    int startPort = 49000;

    public void makeMediaAndServer(int mediaNum, int serverNum) {
        makeMediaAndServer(mediaNum, serverNum, RECORDED, RTSP);
    }

    public void makeMediaAndServer(int mediaNum, int serverNum, BroadCast type) {
        makeMediaAndServer(mediaNum, serverNum, type, RTSP);
    }

    public void makeMediaAndServer(int mediaNum, int serverNum, BroadCast type, MediaProtocol p) {
        for (int i = 0; i < mediaNum; i++) {
            Media media = make(a(MEDIA, with(mediaKey, "" + (mkey + i)),
                    with(mediaPath, "/a/b/x" + i), with(RainMaker.mediaName, "a" + i)));
            mediaRepo.save(media);
        }
        for (int i = 0; i < serverNum; i++) {
            MediaServer server = make(a(MEDIA_SERVER, with(RainMaker.serverHost, "127.0.0." + i),
                    with(RainMaker.serverPort, startPort + i), with(RainMaker.serverType, type),
                    with(RainMaker.serverProtocol, p)));
            serverRepo.save(server);
        }
    }

}
