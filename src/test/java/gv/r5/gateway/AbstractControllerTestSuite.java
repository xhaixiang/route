package gv.r5.gateway;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"classpath:spring/appServlet/servlet-context.xml","classpath:spring/root-context.xml"})
@Transactional
@ActiveProfiles("dev")
public abstract class AbstractControllerTestSuite {
    @Autowired
    protected WebApplicationContext wac;

    protected MockMvc mockMvc;

    protected String gatewayPrefix = "/gateway/v1";
    protected String routePrefix = "/route/v1";
    protected String serverPrefix = "/server/v1";

    @Before
    public void setup(){
        mockMvc = webAppContextSetup(wac).build();
    }

}
