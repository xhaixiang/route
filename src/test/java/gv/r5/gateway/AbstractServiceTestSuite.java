package gv.r5.gateway;

import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/appServlet/servlet-context.xml","classpath:spring/root-context.xml"})
@Transactional
@ActiveProfiles("dev")
public class AbstractServiceTestSuite {
}
