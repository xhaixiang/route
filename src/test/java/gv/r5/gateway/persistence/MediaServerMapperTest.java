package gv.r5.gateway.persistence;


import gv.r5.gateway.AbstractMapperTestSuite;
import gv.r5.gateway.domain.MediaProtocol;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.domain.LifeCycle;
import org.hamcrest.core.Is;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class MediaServerMapperTest extends AbstractMapperTestSuite {
    @Autowired
    private MediaServerMapper mapper;

    @Test
    public void insert() {
        MediaServer t = new MediaServer("123", "192.168.3.2", 49001, 10);
        long insert = mapper.insert(t);
        assertTrue(insert == 1);

        MediaServer s = mapper.get(t.getId());
        assertEquals("123", s.getName());

        s = mapper.getActivedServer("123");
        assertEquals("192.168.3.2", s.getHost());
        assertEquals(49001, s.getPort());
        assertEquals(10, s.getSize());
    }

    @Test
    public void insert_multi_succ() {
        MediaServer t = new MediaServer("123", "192.168.3.2", 49001, 10);
        long insert = mapper.insert(t);
        t.setName("a");
        mapper.insert(t);
        int a = t.getId().intValue();
        t.setName("b");
        mapper.insert(t);
        assertEquals(1, t.getId().intValue() - a);
    }

    @Test
    public void get_by_name_null_if_not_found() {
        MediaServer s = mapper.getActivedServer("kdjds");
        assertNull(s);
    }

    @Test
    public void remove_success() {
        MediaServer t = insert_remove_media_server();
        MediaServer abc = mapper.getActivedServer("abc");
        assertNull(abc);
        assertEquals(LifeCycle.UNINSTALLED, mapper.get(t.getId()).getStatus());

    }

    @Test
    public void starting_success() {
        MediaServer t = insert_remove_media_server();
        t.setHost("127.0.0.1");
        t.setThreshold(1000);
        t.setProtocol(MediaProtocol.BOTH);
        mapper.active(t);
        MediaServer abc = mapper.getActivedServer("abc");
        assertThat(abc.getHost(), IsEqual.equalTo("127.0.0.1"));
        assertThat(abc.getProtocol(), Is.is(MediaProtocol.BOTH));
    }

    private MediaServer insert_remove_media_server() {
        MediaServer t = new MediaServer("abc", "192.168.3.2", 49001, 10);
        mapper.insert(t);
        mapper.remove(t.getId());
        return t;
    }

}