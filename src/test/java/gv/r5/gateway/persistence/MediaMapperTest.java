package gv.r5.gateway.persistence;

import gv.r5.gateway.AbstractMapperTestSuite;
import gv.r5.gateway.domain.Media;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MediaMapperTest extends AbstractMapperTestSuite {

    @Autowired
    private MediaMapper mapper;

    @Test
    public void insert() {
        Media v = new Media("key-1", "/a/b/c/1.mp4");
        mapper.insert(v);
        v = new Media("key-2", "/a/b/c/2.mp4");
        v.setName("abc");
        mapper.insert(v);
        Media media = mapper.get(v.getId());
        assertEquals(media.getKey(), v.getKey());
        assertEquals("abc", media.getName());
    }

    @Test
    public void remove() {
        Media v = new Media("key-1", "/a/b/c/1.mp4");
        mapper.insert(v);
        Media mediaVerify = mapper.getByKey("key-1");
        mapper.remove(v.getId());
        assertNull(mapper.getByKey("key-1"));
    }
}
