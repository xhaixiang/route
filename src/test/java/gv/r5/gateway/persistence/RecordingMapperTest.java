package gv.r5.gateway.persistence;

import gv.r5.gateway.AbstractMapperTestSuite;
import gv.r5.gateway.domain.LifeCycle;
import gv.r5.gateway.domain.MediaProtocol;
import gv.r5.gateway.domain.Recording;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Created by xuhaixiang on 9/13/14.
 */
public class RecordingMapperTest extends AbstractMapperTestSuite {

    @Autowired
    RecordingMapper recordingMapper;

    @Test
    public void recording() {
        Recording r = new Recording(123L);
        r.setEndDate(new Date());
        r.setProtocol(MediaProtocol.RTMP);
        r.setStartDate(new Date());
        r.setStreamUrl("rtmp://x.x.x.x.x");
        r.setStatus(LifeCycle.INSTALLED);
        recordingMapper.insert(r);
        assertEquals(r.getId(), new Long(1));
        recordingMapper.start(r.getId());
        recordingMapper.finish(r.getId());
        assertEquals(r.getId(), recordingMapper.isRecording(r));
        recordingMapper.list();
    }
}
