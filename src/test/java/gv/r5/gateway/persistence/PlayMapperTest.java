package gv.r5.gateway.persistence;

import gv.r5.gateway.AbstractMapperTestSuite;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.domain.Playing;
import gv.r5.gateway.domain.Media;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PlayMapperTest extends AbstractMapperTestSuite {

    @Autowired
    private PlayMapper mapper;

    @Test
    public void insert() {
        Playing p = new Playing(1234L, 1L);
        mapper.insert(p);
        p = new Playing(2345L, 1L);
        mapper.insert(p);
        p = new Playing(1234L, 2L);
        mapper.insert(p);
        p = new Playing(3456L, 1L);
        mapper.insert(p);
        p = new Playing(4567L, 1L);
        mapper.insert(p);

        List<Playing> byServer = mapper.findByServer(1L);
        assertEquals(4, byServer.size());
        List<Playing> byVideo = mapper.findByVideo(1234L);
        assertEquals(2, byVideo.size());

    }

    @Test
    public void findNull() {
        List<Playing> djdk = mapper.findByServer(72262L);
        assertTrue(djdk.isEmpty());
        List<Playing> abkdkd = mapper.findByVideo(38383333L);
        assertTrue(abkdkd.isEmpty());
    }
}
