package gv.r5.gateway.persistence;

import gv.r5.gateway.AbstractMapperTestSuite;
import gv.r5.gateway.domain.LifeCycle;
import gv.r5.gateway.domain.LiveMedia;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class LiveMediaMapperTest extends AbstractMapperTestSuite {

    @Autowired
    private LiveMediaMapper liveMediaMapper;

    @Test
    public void insert() {
        LiveMedia liveMedia = new LiveMedia();
        liveMedia.setStatus(LifeCycle.INSTALLED);
        liveMedia.setStreamUrl("rtmp://x/x/x/x/x");
        liveMedia.setCreateDate(new Date());
        liveMedia.setKey("aaaaaaa");
        liveMedia.setName("bbbbb");
        liveMediaMapper.insert(liveMedia);
        liveMediaMapper.get(liveMedia.getId());
        LiveMedia aaaaaaa = liveMediaMapper.getByKey("aaaaaaa");
        assertEquals(aaaaaaa.getName(), "bbbbb");
    }
}
