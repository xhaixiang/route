<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <title>fileupload | mvc-showcase</title>
    </head>
    <body>
        <div id="fileuploadContent">
        <h2>File Upload</h2>
        <c:url var="post_url" value="/route/v1/upload/stream" />
        <form action="${post_url}" method="POST" enctype="multipart/form-data">
        <label for="file">File</label>
        <input id="file" type="file" name="file" />
        <p><button type="submit">Upload</button></p>
        </form>
        </div>
    </body>
</html>
