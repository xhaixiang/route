package gv.r5.gateway.exceptions;

public class RedValidateException extends RuntimeException {
    public RedValidateException(String s) {
        super(s);
    }
}
