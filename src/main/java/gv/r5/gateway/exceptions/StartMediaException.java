package gv.r5.gateway.exceptions;

import gv.r5.gateway.domain.LiveMedia;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.remote.SimpleResult;

public class StartMediaException extends RuntimeException {

    public StartMediaException(Media media, SimpleResult result) {
        super(media.toString()+":"+result.toString());
    }

    public StartMediaException(LiveMedia media, SimpleResult result) {
        super(media.toString()+":"+result.toString());
    }
}
