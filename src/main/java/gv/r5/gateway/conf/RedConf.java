package gv.r5.gateway.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RedConf {

    @Value(value = "${upload.root}")
    private String uploadRoot;

    @Value(value = "${script.cmd.root}")
    private String cmdScriptRoot;

    @Value(value = "${server.schedule:false}")
    private boolean isSchedule;

    public boolean isSchedule() {
        return isSchedule;
    }

    public void setSchedule(boolean isSchedule) {
        this.isSchedule = isSchedule;
    }

    public String getUploadRoot() {
        return uploadRoot;
    }

    public void setUploadRoot(String uploadRoot) {
        this.uploadRoot = uploadRoot;
    }

    public String getCmdScriptRoot() {
        return cmdScriptRoot;
    }

    public void setCmdScriptRoot(String cmdScriptRoot) {
        this.cmdScriptRoot = cmdScriptRoot;
    }

    @Override
    public String toString() {
        return "RedConf{" +
                "uploadRoot='" + uploadRoot + '\'' +
                ", cmdScriptRoot='" + cmdScriptRoot + '\'' +
                ", isSchedule=" + isSchedule +
                '}';
    }
}
