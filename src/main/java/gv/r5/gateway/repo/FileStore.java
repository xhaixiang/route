package gv.r5.gateway.repo;

import gv.r5.gateway.conf.RedConf;
import gv.r5.gateway.domain.Media;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tom.ftp.util.TomFile;
import tom.script.util.HDFSCmd;

import java.io.*;

@Repository
public class FileStore {

    private Logger logger = LoggerFactory.getLogger(FileStore.class);

    @Autowired
    RedConf redConf;

    public void store(Media m, InputStream inputStream) throws IOException {
        FileOutputStream fos = getFileOutputStream(m);
        IOUtils.copy(inputStream, fos);
        IOUtils.closeQuietly(fos);
        uploadToHdfs(m);
    }

    private void uploadToHdfs(Media m) {
        logger.info("upload to hdfs: {} with conf: {}", m, redConf);
        HDFSCmd su = new HDFSCmd(redConf.getCmdScriptRoot());
        File f = new File(redConf.getUploadRoot());
        f = new File(f, m.getPath());
        f = new File(f, m.getName());
        su.upload(f, m.getPath());
    }

    private FileOutputStream getFileOutputStream(Media m) throws IOException {
        File f = new File(redConf.getUploadRoot());
        f = new File(f, m.getPath());
        f = new File(f, m.getName());
        return FileUtils.openOutputStream(f);
    }

    public void storeFtp(Media media, TomFile file) throws IOException {
        FileOutputStream fos = getFileOutputStream(media);
        try {
            file.writeTo(fos);
        } finally {
            fos.close();
        }
        uploadToHdfs(media);
    }

    public byte[] open(String filePath) throws IOException {
        HDFSCmd hdfsCmd = new HDFSCmd(redConf.getCmdScriptRoot());
        File tmpDir = FileUtils.getTempDirectory();
        String name = FilenameUtils.getName(filePath);
        String path = FilenameUtils.getPath(filePath);
        File tmpFile = FileUtils.getFile(tmpDir, name);
        logger.info("prepare open {}", tmpFile);
        logger.info("download from hdfs {}, {},{}", path, name, tmpFile);
        hdfsCmd.downaload("/"+path, name, tmpFile);
        logger.info("Open tmp file stream");
        FileInputStream fileInputStream = FileUtils.openInputStream(tmpFile);
        byte[] array = IOUtils.toByteArray(fileInputStream);
        IOUtils.closeQuietly(fileInputStream);
        return array;
    }
}
