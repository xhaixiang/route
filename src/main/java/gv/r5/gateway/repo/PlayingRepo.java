package gv.r5.gateway.repo;

import com.google.common.collect.Maps;
import gv.r5.gateway.domain.Playing;
import gv.r5.gateway.persistence.PlayMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class PlayingRepo {

    private Logger log = LoggerFactory.getLogger(PlayingRepo.class);

    @Autowired
    private PlayMapper mapper;

    public Playing save(Playing p) {
        mapper.insert(p);
        return p;
    }

    public boolean checkRecent(Long media, Long serverId) {
        Map<String, Long> cond = Maps.newHashMap();
        cond.put("media", media);
        cond.put("server", serverId);
        int count = mapper.recentMediaInServer(cond);
        log.debug("recent playing {} in server {}, result {}", new Object[]{media, serverId, count});
        return count > 0;
    }

    public int running(Long serverId) {
        return mapper.running(serverId);
    }

    public boolean checkRecentLive(String liveUrl, Long id) {
        Map<String, String> cond = Maps.newHashMap();
        cond.put("liveUrl", liveUrl);
        cond.put("server", ""+id);
        int count = mapper.recentLiveStreamInServer(cond);
        log.debug("recent playing {} in server {}, result {}", new Object[]{liveUrl, id, count});
        return count > 0;
    }
}
