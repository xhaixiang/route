package gv.r5.gateway.repo;

import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.persistence.MediaServerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MediaServerRepo {
    @Autowired
    private MediaServerMapper mapper;

    public Long save(MediaServer s) {
        return mapper.insert(s);
    }

    public void remove(String name) {
        MediaServer byName = mapper.getActivedServer(name);
        if(byName != null){
            mapper.remove(byName.getId());
        }
    }

    public MediaServer get(Long serverId) {
        return mapper.get(serverId);
    }

    public List<MediaServer> list() {
        return mapper.list();
    }

    public void active(MediaServer server) {
        MediaServer byName = mapper.getByName(server.getName());
        byName.update(server);
        if(byName != null){
            mapper.active(byName);
        }
    }

    public MediaServer getByName(String name) {
        return mapper.getByName(name);
    }

    public void refresh(Long id) {
        mapper.statusRefresh(id);
    }

    public List<MediaServer> listLiveServer(){
        return mapper.listLive();
    }

    public List<MediaServer> listRecordServer(){
        return mapper.listRecord();
    }
}
