package gv.r5.gateway.repo;

import gv.r5.gateway.domain.LifeCycle;
import gv.r5.gateway.domain.LiveMedia;
import gv.r5.gateway.domain.generator.MediaKeyGen;
import gv.r5.gateway.domain.generator.MediaNameGen;
import gv.r5.gateway.persistence.LiveMediaMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class LiveMediaRepo {
    @Autowired
    private LiveMediaMapper liveMediaMapper;

    public LiveMedia create(String addr) {
        LiveMedia lv = new LiveMedia();
        lv.setKey(new MediaKeyGen().gen());
        lv.setName(new MediaNameGen().gen(""));
        lv.setCreateDate(new Date());
        lv.setStatus(LifeCycle.INSTALLED);
        lv.setStreamUrl(addr);
        return lv;
    }

    public void save(LiveMedia lv) {
        liveMediaMapper.insert(lv);
    }
}
