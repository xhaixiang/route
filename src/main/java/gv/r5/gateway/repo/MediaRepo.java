package gv.r5.gateway.repo;

import com.google.common.collect.Maps;
import gv.r5.gateway.domain.LifeCycle;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaCondition;
import gv.r5.gateway.persistence.MediaMapper;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.List;

@Repository
public class MediaRepo {

    @Autowired
    private MediaMapper mapper;

    public void save(Media v) {
        mapper.insert(v);
    }

    public Media getByKey(String key) {
        Media media = mapper.getByKey(key);
        Assert.notNull(media, "media not found: " + key);
        return media;
    }

    public Media getByKeyIgnoreStatus(String key) {
        Media media = mapper.getByKeyIgnoreStatus(key);
        Assert.notNull(media, "media not found: " + key);
        return media;
    }

    public Media get(Long mediaId) {
        return mapper.get(mediaId);
    }

    public List<Media> list() {
        return mapper.list();
    }

    public Media create(String fileName, String path, String extName){
        Media m = new Media(fileName, path, extName);
        m.generateKeyAndPath();
        m.setStatus(LifeCycle.INSTALLED);
        return m;
    }

    public Media createRecordingMedia(String streamUrl){
        Media m = new Media();
        m.setFilePath(streamUrl);
        m.generateKeyAndPath();
        m.setStatus(LifeCycle.INSTALLED);
        return m;
    }

    public Media create(String fileName, String path){
        return create(fileName, path, FilenameUtils.getExtension(fileName));
    }

    public void binding(String key) {
        HashMap<String, String> map = Maps.newHashMap();
        map.put("key", key);
        map.put("status", LifeCycle.ACTIVE.toString());
        mapper.updateStatus(map);
    }

    public void unbinding(String key) {
        HashMap<String, String> map = Maps.newHashMap();
        map.put("key", key);
        map.put("status", LifeCycle.INSTALLED.toString());
        mapper.updateStatus(map);
    }

    public List<Media> query(MediaCondition mq) {
        return mapper.find(mq);
    }
}
