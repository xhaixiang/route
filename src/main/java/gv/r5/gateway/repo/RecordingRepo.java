package gv.r5.gateway.repo;

import gv.r5.gateway.domain.LifeCycle;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaProtocol;
import gv.r5.gateway.domain.Recording;
import gv.r5.gateway.persistence.RecordingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public class RecordingRepo {

    @Autowired
    private RecordingMapper recordingMapper;

    public Recording create(Media m, String streamUrl, Date start, Date end) {
        Recording r = new Recording(m.getId());
        r.setStreamUrl(streamUrl);
        r.setStartDate(start);
        r.setEndDate(end);
        r.setProtocol(MediaProtocol.RTMP);
        r.setStatus(LifeCycle.INSTALLED);
        return r;
    }

    public void save(Recording r) {
        recordingMapper.insert(r);
    }

    public List<Recording> list() {
        return recordingMapper.list();
    }

    public void cancel(Recording r) {
        recordingMapper.remove(r.getId());
    }

    public void finish(Long mediaId) {
        recordingMapper.finish(mediaId);
    }

    public void start(Long mediaId) {
        recordingMapper.start(mediaId);
    }
}
