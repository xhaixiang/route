package gv.r5.gateway.domain;

import java.util.Date;

public class Recording {
    private Long id;
    private Long mediaId;
    private Date startDate;
    private Date endDate;
    private String streamUrl;
    private MediaProtocol protocol;
    private LifeCycle status;

    public Recording() {
    }

    public Recording(Long mediaId) {
        this.mediaId = mediaId;
    }

    public MediaProtocol getProtocol() {
        return protocol;
    }

    public void setProtocol(MediaProtocol protocol) {
        this.protocol = protocol;
    }

    public Long getMediaId() {
        return mediaId;
    }

    public void setMediaId(Long mediaId) {
        this.mediaId = mediaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    public LifeCycle getStatus() {
        return status;
    }

    public void setStatus(LifeCycle status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Recording{" +
                "id=" + id +
                ", mediaId=" + mediaId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", streamUrl='" + streamUrl + '\'' +
                ", protocol=" + protocol +
                ", status=" + status +
                '}';
    }
}
