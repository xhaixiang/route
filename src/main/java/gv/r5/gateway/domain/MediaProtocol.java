package gv.r5.gateway.domain;

public enum MediaProtocol {
    RTMP{
        @Override
        public String getProtocolString() {
            return "rtmp";
        }
    },
    RTSP{
        @Override
        public String getProtocolString() {
            return "rtsp";
        }
    },
    BOTH{
        @Override
        public String getProtocolString() {
            return "rtmp";
        }
    };

    private MediaProtocol(){

    }

    public String getProtocolString(){
        return "";
    }
}
