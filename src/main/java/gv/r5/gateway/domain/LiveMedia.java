package gv.r5.gateway.domain;

import java.util.Date;

/**
 * Created by xuhaixiang on 9/13/14.
 */
public class LiveMedia {

    private Long id;
    private String key;
    private String name;
    private String streamUrl;
    private Date createDate;
    private LifeCycle status;

    public LiveMedia() {
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public LifeCycle getStatus() {
        return status;
    }

    public void setStatus(LifeCycle status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    @Override
    public String toString() {
        return "LiveMedia{" +
                "id=" + id +
                ", key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", streamUrl='" + streamUrl + '\'' +
                ", createDate=" + createDate +
                ", status=" + status +
                '}';
    }
}
