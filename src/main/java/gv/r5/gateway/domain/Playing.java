package gv.r5.gateway.domain;

import java.util.Date;

public class Playing {
    private Long id;
    private Long serverId;
    private Long mediaId;
    private String liveUrl;
    private String address;
    private Date startDate = new Date();
    private BroadCast broadCast = BroadCast.RECORDED;
    private LifeCycle status = LifeCycle.ACTIVE;

    public Playing() {
    }

    public Playing(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public Playing(Long mediaId, Long serverId) {
        this.mediaId = mediaId;
        this.serverId = serverId;
    }

    public Playing(Media m, MediaServer s) {
        this.mediaId = m.getId();
        this.serverId = s.getId();
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LifeCycle getStatus() {
        return status;
    }

    public void setStatus(LifeCycle status) {
        this.status = status;
    }

    public Long getMediaId() {
        return mediaId;
    }

    public void setMediaId(Long mediaId) {
        this.mediaId = mediaId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public BroadCast getBroadCast() {
        return broadCast;
    }

    public void setBroadCast(BroadCast broadCast) {
        this.broadCast = broadCast;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    @Override
    public String toString() {
        return "Playing{" +
                "id=" + id +
                ", serverId=" + serverId +
                ", mediaId=" + mediaId +
                ", liveUrl='" + liveUrl + '\'' +
                ", address='" + address + '\'' +
                ", startDate=" + startDate +
                ", broadCast=" + broadCast +
                ", status=" + status +
                '}';
    }
}
