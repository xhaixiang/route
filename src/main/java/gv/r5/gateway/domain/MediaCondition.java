package gv.r5.gateway.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MediaCondition {

    private String status;
    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)
    private Date date;
    private String addr;

    private String dateStr;

    public MediaCondition() {
    }

    public String getDateStr() {
        if(date != null){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.format(date);
        }
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    @Override
    public String toString() {
        return "MediaCondition{" +
                "status='" + status + '\'' +
                ", date='" + date + '\'' +
                ", addr='" + addr + '\'' +
                '}';
    }
}
