package gv.r5.gateway.domain;

import gv.r5.gateway.domain.generator.MediaKeyGen;
import gv.r5.gateway.domain.generator.MediaNameGen;
import gv.r5.gateway.domain.generator.MediaPathGen;

import java.util.Date;

/**
 * Created by xuhaixiang on 7/9/14.
 */
public class Media {
    private Long id;
    private String name;
    private String key;
    private String path;
    private String fileName;
    private String filePath;
    private String extName;
    private Date createDate = new Date();
    private LifeCycle status = LifeCycle.ACTIVE;

    public Media() {
    }

    public Media(String key, String path) {
        this.key = key;
        this.path = path;
    }

    public Media(String fileName, String path, String extName) {
        this.fileName = fileName;
        this.filePath = path;
        this.extName = extName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getExtName() {
        return extName;
    }

    public void setExtName(String extName) {
        this.extName = extName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public LifeCycle getStatus() {
        return status;
    }

    public void setStatus(LifeCycle status) {
        this.status = status;
    }

    public void generateKeyAndPath() {
        this.setKey(new MediaKeyGen().gen());
        this.setName(new MediaNameGen().gen(this.getExtName()));
        this.setPath(new MediaPathGen().gen());
    }

    @Override
    public String toString() {
        return "Media{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", path='" + path + '\'' +
                ", fileName='" + fileName + '\'' +
                ", filePath='" + filePath + '\'' +
                ", extName='" + extName + '\'' +
                ", createDate=" + createDate +
                ", status=" + status +
                '}';
    }
}
