package gv.r5.gateway.domain;

/**
 * Created by xuhaixiang on 7/10/14.
 */
public enum BroadCast {
    RECORDED, LIVE, BOTH
}
