package gv.r5.gateway.domain;

public enum LifeCycle {
    INSTALLED, UNINSTALLED, RESOLVED, STARTING, STOPPING, ACTIVE
}
