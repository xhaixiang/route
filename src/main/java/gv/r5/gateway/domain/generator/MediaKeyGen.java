package gv.r5.gateway.domain.generator;

import java.util.UUID;

/**
 * Created by xuhaixiang on 8/18/14.
 */
public class MediaKeyGen {
    public String gen() {
        return UUID.randomUUID().toString();
    }
}
