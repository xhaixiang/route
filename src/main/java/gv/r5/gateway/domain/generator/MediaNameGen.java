package gv.r5.gateway.domain.generator;

import org.springframework.util.StringUtils;

import java.util.UUID;

public class MediaNameGen {
    public String gen(String extName) {
        if(StringUtils.hasText(extName)){
            return UUID.randomUUID().toString()+"."+extName;
        }else{
            return UUID.randomUUID().toString();
        }
    }
}
