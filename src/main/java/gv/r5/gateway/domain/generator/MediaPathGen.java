package gv.r5.gateway.domain.generator;

import java.io.File;
import java.util.Calendar;

public class MediaPathGen {

    public String gen() {
        Calendar c = Calendar.getInstance();
        return "/"+c.get(Calendar.YEAR)+ "/"+c.get(Calendar.MONTH)+"/"+c.get(Calendar.DAY_OF_MONTH);
    }
}
