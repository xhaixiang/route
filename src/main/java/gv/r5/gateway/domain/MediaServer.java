package gv.r5.gateway.domain;

import gv.r5.gateway.exceptions.StartMediaException;
import gv.r5.gateway.remote.AliceAdaptor;
import gv.r5.gateway.remote.RemoteProxy;
import gv.r5.gateway.remote.SimpleResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MediaServer {
    private Logger log = LoggerFactory.getLogger(MediaServer.class);

    private Long id;
    private String name;
    private String host;
    private int port;
    private int size = 100;
    private int threshold = 80;
    private MediaProtocol protocol = MediaProtocol.RTSP;
    private Date createDate = new Date();
    private Date updateDate = new Date();

    private BroadCast broadCast = BroadCast.RECORDED;
    private LifeCycle status = LifeCycle.ACTIVE;

    public MediaServer() {
    }

    public MediaServer(String name, String host, int port, int limitSize) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.size = limitSize;
    }

    public MediaProtocol getProtocol() {
        return protocol;
    }

    public void setProtocol(MediaProtocol protocol) {
        this.protocol = protocol;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public BroadCast getBroadCast() {
        return broadCast;
    }

    public void setBroadCast(BroadCast broadCast) {
        this.broadCast = broadCast;
    }

    public LifeCycle getStatus() {
        return status;
    }

    public void setStatus(LifeCycle status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "MediaServer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", size=" + size +
                ", threshold=" + threshold +
                ", protocol=" + protocol +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                ", broadCast=" + broadCast +
                ", status=" + status +
                '}';
    }

    public void update(MediaServer server) {
        this.setBroadCast(server.getBroadCast());
        this.setHost(server.getHost());
        this.setPort(server.getPort());
        this.setThreshold(server.getThreshold());
        this.setProtocol(server.getProtocol());
        this.setSize(server.getSize());
        this.setUpdateDate(new Date());
    }

    public boolean isActive() {
        return status == LifeCycle.ACTIVE;
    }

    public String getServerUrl() {
        return "http://" + getHost() + ":" + getPort();
    }

    public String capture(Media m, Long captureTime, RemoteProxy client){
        AliceAdaptor aliceAdaptor = client.get(AliceAdaptor.class, getServerUrl());
        SimpleResult result = aliceAdaptor.capture(m, captureTime);
        if(result.isOK()){
            return result.getStreamUrl();
        }else{
            throw new StartMediaException(m, result);
        }
    }

    public Playing start(final Media media, String frame, RemoteProxy client) {
        SimpleResult result = client.get(AliceAdaptor.class, getServerUrl()).start(media, frame);
        if (result.isOK()) {
            Playing p = new Playing();
            p.setAddress(result.getStreamUrl());
            p.setMediaId(media.getId());
            p.setServerId(this.getId());
            return p;
        }
        throw new StartMediaException(media, result);
    }

    public Playing live(LiveMedia media, Long duration, String frame, RemoteProxy client) {
        SimpleResult result = client.get(AliceAdaptor.class, getServerUrl()).live(media, duration, frame);
        if (result.isOK()) {
            Playing p = new Playing();
            p.setAddress(result.getStreamUrl());
            p.setServerId(this.getId());
            p.setLiveUrl(media.getStreamUrl());
            p.setMediaId(media.getId());
            return p;
        }
        throw new StartMediaException(media, result);
    }

    public void recording(Media media, Recording r, RemoteProxy client) {
        long l = r.getEndDate().getTime() - r.getStartDate().getTime();
        SimpleResult result = client.get(AliceAdaptor.class, getServerUrl()).recording(media, r.getStreamUrl(), l/1000);
    }
}
