package gv.r5.gateway.persistence;

import gv.r5.gateway.domain.Playing;

import java.util.List;
import java.util.Map;

public interface PlayMapper {
    public Long insert(Playing p);

    public List<Playing> findByServer(Long serverId);

    public List<Playing> findByVideo(Long videoId);

    public Playing recentPlaying(String key);

    public int recentMediaInServer(Map<String,Long> cond);

    public int running(Long server);

    public int recentLiveStreamInServer(Map<String, String> cond);
}
