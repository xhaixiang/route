package gv.r5.gateway.persistence;

import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaCondition;

import java.util.HashMap;
import java.util.List;

/**
 * Created by xuhaixiang on 7/9/14.
 */
public interface MediaMapper {

    public Long insert(Media v);

    public Media get(Long id);

    public Media getByKey(String key);

    public Media getByKeyIgnoreStatus(String key);

    public void remove(Long id);

    public List<Media> list();

    public void updateStatus(HashMap<String, String> map);

    public List<Media> find(MediaCondition mq);
}
