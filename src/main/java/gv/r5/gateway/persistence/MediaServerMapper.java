package gv.r5.gateway.persistence;


import gv.r5.gateway.domain.MediaServer;

import java.util.List;

public interface MediaServerMapper {

    public Long insert(MediaServer t);

    public MediaServer get(Long id);

    public MediaServer getActivedServer(String name);

    public void remove(Long id);

    public void suspend(Long id);

    public List<MediaServer> list();

    public void active(MediaServer server);

    public MediaServer getByName(String name);

    public void statusRefresh(Long id);

    public List<MediaServer> listLive();

    public List<MediaServer> listRecord();
}
