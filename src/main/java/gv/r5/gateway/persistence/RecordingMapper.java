package gv.r5.gateway.persistence;

import gv.r5.gateway.domain.Recording;

import java.util.List;

public interface RecordingMapper {

    public void insert(Recording r);

    public int start(Long id);

    public Long isRecording(Recording r);

    public int finish(Long id);

    public int remove(Long id);

    public List<Recording> list();
}
