package gv.r5.gateway.persistence;

import gv.r5.gateway.domain.LiveMedia;

public interface LiveMediaMapper {

    public Long insert(LiveMedia v);

    public LiveMedia get(Long id);

    public LiveMedia getByKey(String key);

}
