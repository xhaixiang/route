package gv.r5.gateway.core;

import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.repo.PlayingRepo;
import org.springframework.util.Assert;

public class ServerRating {

    private Long media;
    private String liveUrl;
    private MediaServer server;
    private double value;

    public ServerRating(Long media, MediaServer server) {
        this.media = media;
        this.server = server;
    }

    public ServerRating(String liveUrl, MediaServer server) {
        this.liveUrl = liveUrl;
        this.server = server;
    }

    public static ServerRating rating(Long media, MediaServer server,
                                      RatingStrategy ratingStrategy, PlayingRepo playingRepo) {
        ServerRating r = new ServerRating(media, server);
        r.checkRecorde(ratingStrategy, playingRepo);
        return r;
    }

    public static ServerRating liveRating(String addr, MediaServer server,
                                      RatingStrategy ratingStrategy, PlayingRepo playingRepo) {
        ServerRating r = new ServerRating(addr, server);
        r.checkLive(ratingStrategy, playingRepo);
        return r;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    public Long getMedia() {
        return media;
    }

    public void setMedia(Long media) {
        this.media = media;
    }

    public MediaServer getServer() {
        return server;
    }

    public void setServer(MediaServer server) {
        this.server = server;
    }

    private ServerRating checkRecorde(RatingStrategy ratingStrategy, PlayingRepo playingRepo) {
        double mediaPercent = playingRepo.checkRecent(media, getServer().getId()) ? 1 : 0;
        return check(mediaPercent, ratingStrategy, playingRepo);
    }

    private ServerRating checkLive(RatingStrategy ratingStrategy, PlayingRepo playingRepo) {
        double mediaPercent = playingRepo.checkRecentLive(liveUrl, getServer().getId()) ? 1 : 0;
        return check(mediaPercent, ratingStrategy, playingRepo);
    }

    private ServerRating check(double mediaPercent, RatingStrategy ratingStrategy, PlayingRepo playingRepo) {
        int running = running(playingRepo);
        Assert.isTrue((getServer().getSize() - running) > 0, "exceed limit." + getServer().getSize() + ":" + running);
        double systemInfoValue = getSystemBalance();
        double limitPercent = running / getServer().getSize();
        setValue(ratingStrategy.calculate(new double[]{mediaPercent, systemInfoValue, limitPercent}, running > getServer().getThreshold()));
        return this;
    }

    private int running(PlayingRepo playingRepo) {
        return playingRepo.running(getServer().getId());
    }

    public double getSystemBalance() {
        return Math.random() / 2.0;
    }

    @Override
    public String toString() {
        return "ServerRating{" +
                "media=" + media +
                ", liveUrl='" + liveUrl + '\'' +
                ", server=" + server +
                ", value=" + value +
                '}';
    }
}