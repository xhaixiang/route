package gv.r5.gateway.core;

import com.google.common.collect.Ordering;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.Comparator;

@Component
public class Evaluator {

    private static final Logger logger = LoggerFactory.getLogger(Evaluator.class);

    @Autowired
    private ServerPool pool;

    public Evaluator() {
    }

    public Evaluator(ServerPool serverPool) {
        pool = serverPool;
    }

    public MediaServer live(final String streamUrl) {
        logger.info("Evaluator live server with stream url : {}", streamUrl);
        Iterable<ServerRating> ratings = pool.ratingsLiveServer(streamUrl);
        Assert.isTrue(ratings.iterator().hasNext(), "no server available");
        return maxRating(ratings);
    }

    public MediaServer recorded(final Media media) {
        logger.info("Evaluator recorded server with media {}", media);
        Iterable<ServerRating> ratings = pool.ratingsMediaServer(media.getId());
        Assert.isTrue(ratings.iterator().hasNext(), "no server available");
        return maxRating(ratings);
    }

    private MediaServer maxRating(Iterable<ServerRating> ratings) {
        ServerRating max = Ordering.from(new Comparator<ServerRating>() {
            @Override
            public int compare(ServerRating o1, ServerRating o2) {
                int v1 = (o1 == null) ? 0 : (int) Math.round(o1.getValue() * 100);
                int v2 = (o2 == null) ? 0 : (int) Math.round(o2.getValue() * 100);
                return v1 - v2;
            }
        }).max(ratings);
        Assert.notNull(max, "no server avaliable");
        return max.getServer();
    }

}

