package gv.r5.gateway.core;

public class RatingStrategy {

    double[] weight = new double[]{0.3, 0.3, 0.3};
    double[] exceedWeight = new double[]{0.1, 0.1, 0.8};

    public double calculate(double[] values, boolean bAddWeight) {
        double accu = 0.0;
        for (int i = 0; i < values.length; i++) {
            double w = bAddWeight ? exceedWeight[i] : weight[i];
            accu = accu + values[i] * w;
        }
        return accu / 3.0;
    }
}
