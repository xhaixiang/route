package gv.r5.gateway.core;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import gv.r5.gateway.domain.BroadCast;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.repo.MediaServerRepo;
import gv.r5.gateway.repo.PlayingRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ServerPool {

    private Logger log = LoggerFactory.getLogger(ServerPool.class);

    @Autowired
    private MediaServerRepo serverRepo;

    @Autowired
    private PlayingRepo playingRepo;

    public Iterable<ServerRating> ratingsMediaServer(final Long media) {
        List<MediaServer> mediaServers = getMediaServers(BroadCast.RECORDED);
        return Iterables.transform(mediaServers, new Function<MediaServer, ServerRating>() {
            @Override
            public ServerRating apply(MediaServer server) {
                log.debug("rating media {} with {}", media, server);
                try {
                    ServerRating r = ServerRating.rating(media, server, new RatingStrategy(), playingRepo);
                    log.debug("rating server value {}", r);
                    return r;
                } catch (Exception e) {
                    log.warn("rating error", e);
                }
                return null;
            }
        });

    }

    private List<MediaServer> getMediaServers(BroadCast cast) {
        List<MediaServer> mediaServers;
        switch (cast){
            case RECORDED:
                mediaServers = serverRepo.listRecordServer();
                break;
            case LIVE:
                mediaServers = serverRepo.listLiveServer();
                break;
            default:
                mediaServers = serverRepo.list();
                break;
        }
        return mediaServers;
    }

    public Iterable<ServerRating> ratingsLiveServer(final String address) {
        List<MediaServer> mediaServers = getMediaServers(BroadCast.LIVE);
        return Iterables.transform(mediaServers, new Function<MediaServer, ServerRating>() {
            @Override
            public ServerRating apply(MediaServer server) {
                try {
                    ServerRating r = ServerRating.liveRating(address, server, new RatingStrategy(), playingRepo);
                    log.debug("rating server value {}", r);
                    return r;
                } catch (Exception e) {
                    log.warn("rating error", e);
                }
                return null;
            }
        });
    }
}