package gv.r5.gateway.web.extendings;

import gv.r5.gateway.services.TimerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class TimerTaskPostProcesser {

    private Logger logger = LoggerFactory.getLogger(TimerTaskPostProcesser.class);

    @Autowired
    TimerService timerService;

    @PostConstruct
    public void init() {
        try {
            timerService.restartAll();
        } catch (Exception e) {
            logger.error("error.", e);
        }
    }
}
