package gv.r5.gateway.web.extendings;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

@Component
public class MetricsPostProcessor {

    @Autowired
    MetricRegistry metricsRegistry;

    @Autowired
    HealthCheckRegistry healthCheckRegistry;

    @PostConstruct
    public void startReporter(){
        metricsRegistry.registerAll(new MemoryUsageGaugeSet());
        metricsRegistry.registerAll(new ThreadStatesGaugeSet());
        Slf4jReporter reporter = Slf4jReporter.forRegistry(metricsRegistry).outputTo(LoggerFactory.getLogger("Metrics"))
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(1, TimeUnit.HOURS);

    }
}
