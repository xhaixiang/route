package gv.r5.gateway.web.extendings;

import gv.r5.gateway.exceptions.RedValidateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;

@ControllerAdvice
public class GExceptionHandler {

    private Logger logger = LoggerFactory.getLogger("GEH");

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handleRuntimeException(RuntimeException re) {
        logger.error("Error catch ", re);
        return new ResponseEntity<String>("error with runtime exception, such as sql exception."+re.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> handleRuntimeException(IOException re) {
        logger.error("Error catch ", re);
        return new ResponseEntity<String>("error with io exception, such as write file exception."+re.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RedValidateException.class)
    public ResponseEntity<String> handleRuntimeException(RedValidateException re) {
        logger.error("Error catch ", re);
        return new ResponseEntity<String>("error with runtime exception, such as sql exception."+re.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
