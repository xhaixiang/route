package gv.r5.gateway.web.beans;

import gv.r5.gateway.domain.Playing;

public class PlayBean {
    private String address;

    public PlayBean(String address) {
        this.address = address;
    }

    public PlayBean(Playing p) {
        this.address = p.getAddress();
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
