package gv.r5.gateway.web.beans;

import gv.r5.gateway.domain.LifeCycle;
import gv.r5.gateway.domain.Media;

import java.util.Date;

public class MediaBean {
    private String id;

    private String name;

    private String path;

    private String status;

    private Date createDate;

    public MediaBean(String id) {
        this.id = id;
    }

    public MediaBean(Media m){
        this.id = m.getKey();
        this.name = m.getFileName();
        this.path = m.getFilePath();
        String s = (m.getStatus() == LifeCycle.INSTALLED)?"UNBIND":"BIND";
        this.status = s;
        this.createDate = m.getCreateDate();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MediaBean{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", status='" + status + '\'' +
                ", createDate=" + createDate +
                '}';
    }
}
