package gv.r5.gateway.web.controller;

import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.repo.MediaServerRepo;
import gv.r5.gateway.services.RecordingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/server/v1")
public class ServerController {
    private Logger log = LoggerFactory.getLogger(ServerController.class);

    @Autowired
    private MediaServerRepo mediaServerRepo;

    @Autowired
    private RecordingService recordingService;

    @RequestMapping(value = "register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void register(@RequestBody MediaServer server) {
        log.info(server.toString());
        MediaServer byName = mediaServerRepo.getByName(server.getName());
        if (byName != null) {
            if (byName.isActive()) {
                mediaServerRepo.refresh(byName.getId());
            } else {
                mediaServerRepo.active(server);
            }
        } else {
            mediaServerRepo.save(server);
        }
    }

    @RequestMapping(value = "unregister/{name}", method = RequestMethod.POST)
    @ResponseBody
    public void unregister(@PathVariable String name) {
        mediaServerRepo.remove(name);
    }

    @RequestMapping(value = "start/{name}", method = RequestMethod.POST)
    @ResponseBody
    public void startRecording(@PathVariable String name) {
        recordingService.start(name);
    }

    @RequestMapping(value = "finish/{name}", method = RequestMethod.POST)
    @ResponseBody
    public void finishRecording(@PathVariable String name) {
        recordingService.finish(name);
    }

}
