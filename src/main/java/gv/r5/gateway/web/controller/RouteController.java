package gv.r5.gateway.web.controller;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaCondition;
import gv.r5.gateway.domain.Recording;
import gv.r5.gateway.exceptions.RedValidateException;
import gv.r5.gateway.repo.FileStore;
import gv.r5.gateway.repo.MediaRepo;
import gv.r5.gateway.services.*;
import gv.r5.gateway.web.beans.MediaBean;
import gv.r5.gateway.web.beans.RecordingBean;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/route/v1")
public class RouteController {
    private Logger logger = LoggerFactory.getLogger(RouteController.class);

    @Autowired
    MediaRepo mediaRepo;

    @Autowired
    FileStore fileStore;

    @Autowired
    FtpService ftpService;

    @Autowired
    CaptureService captureService;

    @Autowired
    RecordingService recordingService;

    @Autowired
    TimerService timerService;

    @RequestMapping(value = "upload/stream", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MediaBean uploadStream(@RequestParam MultipartFile file, Model model) throws IOException {
        model.addAttribute("message", "File '" + file.getOriginalFilename() + "' uploaded successfully");
        Media media = mediaRepo.create(file.getOriginalFilename(), "", FilenameUtils.getExtension(file.getOriginalFilename()));
        fileStore.store(media, file.getInputStream());
        mediaRepo.save(media);
        logger.info("upload media: {}", media);
        return new MediaBean(media);
    }

    @RequestMapping(value = "upload/ftp", method = RequestMethod.POST)
    @ResponseBody
    public Callable<String> uploadFtp(@RequestParam(value = "addr") final String ftpAddr) throws IOException {
        return new Callable<String>() {
            @Override
            public String call() throws Exception {
                ftpService.store(ftpAddr);
                return "succ";
            }
        };
    }

    @RequestMapping(value = "recording/all", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void recordingAll(@RequestBody List<RecordingBean> uploads) throws IOException {
        logger.info("recording all : {}", uploads);
        timerService.removeAll();
        for (RecordingBean rb : uploads) {
            valRecordingBeanDate(rb);
        }
        for (RecordingBean rb : uploads) {
            for (String stream : rb.getAddress()) {
                recordingService.grab(stream, rb.getStartDate(), rb.getEndDate());
            }
        }
    }

    @RequestMapping(value = "recording/one", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void recordingOne(@RequestBody RecordingBean upload) throws IOException {
        logger.info("recording one : {}", upload);
        valRecordingBeanDate(upload);
        for (String streamUrl : upload.getAddress()) {
            recordingService.grab(streamUrl, upload.getStartDate(), upload.getEndDate());
        }
    }

    private void valRecordingBeanDate(RecordingBean bean) {
        if (bean.getStartDate().before(new Date())) {
            throw new RedValidateException(bean.toString());
        }
        if(bean.getStartDate().after(bean.getEndDate())){
            throw new RedValidateException(bean.toString());
        }
    }

    @RequestMapping(value = "bind/{key}", method = RequestMethod.POST)
    @ResponseBody
    public String binding(@PathVariable String key) {
        mediaRepo.binding(key);
        return "succ";
    }

    @RequestMapping(value = "bind", method = RequestMethod.POST)
    @ResponseBody
    public String binding(@RequestBody List<String> keys) {
        logger.info("binding list : {}", keys);
        for (String key : keys) {
            mediaRepo.binding(key);
        }
        return "succ";
    }

    @RequestMapping(value = "unbind/{key}", method = RequestMethod.POST)
    @ResponseBody
    public String unbinding(@PathVariable String key) {
        mediaRepo.unbinding(key);
        return "succ";
    }

    @RequestMapping(value = "unbind", method = RequestMethod.POST)
    @ResponseBody
    public String unbinding(@RequestBody List<String> keys) {
        logger.info("unbinding list : {}", keys);
        for (String key : keys) {
            mediaRepo.unbinding(key);
        }
        return "succ";
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    @ResponseBody
    public List<MediaBean> medias(MediaCondition mq) {
        logger.info("Query condition:{}", mq);
        return convert(mediaRepo.query(mq));
    }

    @RequestMapping(value = "media/{key}", method = RequestMethod.GET)
    @ResponseBody
    public MediaBean getMedia(@PathVariable String key) {
        logger.info("get media : {}", key);
        Media byKey = mediaRepo.getByKeyIgnoreStatus(key);
        return new MediaBean(byKey);
    }

    private List<MediaBean> convert(List<Media> m) {
        return Lists.transform(m, new Function<Media, MediaBean>() {
            @Override
            public MediaBean apply(Media input) {
                return new MediaBean(input);
            }
        });
    }

    @RequestMapping(value = "capture", method = RequestMethod.GET, produces = MediaType.IMAGE_PNG_VALUE)
    @ResponseBody
    public byte[] capture(@RequestParam(value = "v") String mediaKey, @RequestParam(value = "t", required = false) Long timeDate) throws IOException {
        logger.info("capture key {} time {}", mediaKey, timeDate);
        return captureService.capture(mediaKey, timeDate, TimeUnit.SECONDS);
    }

}
