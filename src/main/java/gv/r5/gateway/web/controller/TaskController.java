package gv.r5.gateway.web.controller;

import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.repo.MediaServerRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Calendar;
import java.util.Date;

@Controller
@RequestMapping("/task/v1")
public class TaskController {
    private Logger log = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private MediaServerRepo mediaServerRepo;

    @RequestMapping(value = "scanServerStatus", method = RequestMethod.POST)
    @ResponseBody
    public void scanServerStatus() {
        for(MediaServer s : mediaServerRepo.list()){
            log.debug("scan server:{}", s);
            if(isOverdue(s.getUpdateDate())){
                log.info("server is overdue {}", s);
                mediaServerRepo.remove(s.getName());
            }
        }
    }

    private boolean isOverdue(Date d) {
        long current = new Date().getTime();
        return current - d.getTime() > 5 * 60 * 1000;
    }

}
