package gv.r5.gateway.web.controller;

import gv.r5.gateway.domain.Playing;
import gv.r5.gateway.services.PlayService;
import gv.r5.gateway.web.beans.PlayBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/gateway/v1")
public class GatewayController {

    @Autowired
    private PlayService playService;

    @RequestMapping(value = "play/record/{recordId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlayBean playRecord(@PathVariable String recordId, @RequestParam(value = "vf", required = false) String frame) {
        Playing result = playService.start(recordId, frame);
        return new PlayBean(result);
    }

    @RequestMapping(value = "play/stream", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlayBean playStream(@RequestParam(value = "addr") String addr, @RequestParam(value = "dt",defaultValue = "3600", required = false) Long duration, @RequestParam(value = "vf", required = false) String frame) {
        Playing result = playService.live(addr, duration, frame);
        return new PlayBean(result);
    }

}
