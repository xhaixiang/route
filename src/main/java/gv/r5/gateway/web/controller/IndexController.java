package gv.r5.gateway.web.controller;

import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.repo.MediaRepo;
import gv.r5.gateway.repo.MediaServerRepo;
import gv.r5.gateway.web.beans.MediaBean;
import gv.r5.gateway.web.beans.RecordingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.codahale.metrics.MetricRegistry.name;

@Controller
public class IndexController {

    @Autowired
    MetricRegistry metricsRegistry;

    @Autowired
    private MediaRepo mediaRepo;

    @Autowired
    private MediaServerRepo mediaServerRepo;

    @RequestMapping("/simple")
    @ResponseBody
    public String count() {
        Counter counter = metricsRegistry.counter(name(IndexController.class, "count"));
        counter.inc();
        return "simple reponse";
    }

    @RequestMapping(value = "servers", method = RequestMethod.GET)
    @ResponseBody
    public List<MediaServer> servers(){
        return mediaServerRepo.list();
    }

    @RequestMapping(value = "medias", method = RequestMethod.GET)
    @ResponseBody
    public List<MediaBean> medias(){
        return convert(mediaRepo.list());
    }

    @RequestMapping(value = "uploadBean", method = RequestMethod.GET)
    @ResponseBody
    public List<RecordingBean> uploadBean(){
        RecordingBean a = new RecordingBean();
        a.setStartDate(new Date());
        a.setEndDate(new Date());
        a.getAddress().add("rtmp://a.b.cn/a/b");
        a.getAddress().add("rtmp://a.b.cn/a/c");
        a.getAddress().add("rtmp://a.b.cn/a/d");
        RecordingBean b = new RecordingBean();
        b.setStartDate(new Date());
        b.setEndDate(new Date());
        b.getAddress().add("rtmp://b.a.cn/a/b");

        List<RecordingBean> result = new ArrayList<>();
        result.add(a);
        result.add(b);
        return result;
    }

    @RequestMapping(value = "upload", method = RequestMethod.GET)
    public String upload(){
        return "home/upload";
    }

    private List<MediaBean> convert(List<Media> m){
        return Lists.transform(m, new Function<Media, MediaBean>() {
            @Override
            public MediaBean apply(Media input) {
                return new MediaBean(input);
            }
        });
    }

}
