package gv.r5.gateway.services;

import gv.r5.gateway.conf.RedConf;
import gv.r5.gateway.core.Evaluator;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.Recording;
import gv.r5.gateway.remote.RemoteProxy;
import gv.r5.gateway.repo.MediaRepo;
import gv.r5.gateway.repo.RecordingRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Timer;

@Service
@Scope(value = "singleton")
public class TimerService {

    private Logger logger = LoggerFactory.getLogger(TimerService.class);

    @Autowired
    private RedConf redConf;

    @Autowired
    private RecordingRepo recordingRepo;

    @Autowired
    private MediaRepo mediaRepo;

    @Autowired
    private RemoteProxy proxy;

    @Autowired
    private Evaluator evaluator;

    private Timer timer;

    public TimerService() {
        timer = new Timer("TM_" + Thread.currentThread().getName());
    }

    private boolean isScheduleServer() {
        return redConf.isSchedule();
    }

    public void restartAll() {
        if (isScheduleServer()) {
            timer.cancel();
            timer.purge();
            timer = new Timer("TM_" + Thread.currentThread().getName());
            for (Recording r : recordingRepo.list()) {
                Media media = mediaRepo.get(r.getMediaId());
                add(new RecordingTask(media, r, proxy, evaluator, recordingRepo));
            }
        }
    }

    @Transactional
    public void removeAll() {
        for (Recording r : recordingRepo.list()) {
            recordingRepo.cancel(r);
        }
    }

    public void add(RecordingTask rt) {
        if (isScheduleServer()) {
            logger.info("add recording to task {}", rt.getDomain());
            timer.schedule(rt, rt.getStartDate());
        }
    }
}
