package gv.r5.gateway.services;

import gv.r5.gateway.domain.Media;
import gv.r5.gateway.repo.FileStore;
import gv.r5.gateway.repo.MediaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tom.ftp.util.FtpFileHandler;
import tom.ftp.util.TomFile;
import tom.ftp.util.TomFtpClient;

import java.io.IOException;

@Service
public class FtpService {

    @Autowired
    MediaRepo mediaRepo;

    @Autowired
    FileStore fileStore;

    public void store(String ftpAddr) throws IOException {
        TomFtpClient.connect(ftpAddr).downloadAll("", new FtpFileHandler() {
            @Override
            public void store(TomFile file) throws IOException {
                Media media = mediaRepo.create(file.getName(), TomFtpClient.touch(file.getRoot(), file.getPath()));
                fileStore.storeFtp(media, file);
                mediaRepo.save(media);
            }
        });
    }
}
