package gv.r5.gateway.services;

import gv.r5.gateway.core.Evaluator;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.domain.Recording;
import gv.r5.gateway.remote.AliceAdaptor;
import gv.r5.gateway.remote.RemoteProxy;
import gv.r5.gateway.repo.RecordingRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.TimerTask;

public class RecordingTask extends TimerTask {
    private Logger logger = LoggerFactory.getLogger(RecordingTask.class);

    private Recording recording;

    private Media media;

    private RemoteProxy proxy;

    private Evaluator evaluator;

    private RecordingRepo recordingRepo;

    public RecordingTask(Media m, Recording r, RemoteProxy proxy, Evaluator evaluator,RecordingRepo recordingRepo){
        this.media = m;
        this.recording = r;
        this.proxy = proxy;
        this.evaluator = evaluator;
        this.recordingRepo = recordingRepo;
    }

    @Override
    public void run() {
        logger.info("run recording task:{}", recording);
        try {
            MediaServer server = evaluator.recorded(media);
            logger.info("choose server for recording {}", server);
            recordingRepo.start(media.getId());
            server.recording(media, recording, proxy);
        } catch (Exception e) {
            logger.error("recording error. ", e);
        }

    }

    public Date getStartDate() {
        return recording.getStartDate();
    }

    public Recording getDomain() {
        return recording;
    }
}
