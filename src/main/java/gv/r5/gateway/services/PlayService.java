package gv.r5.gateway.services;

import gv.r5.gateway.core.Evaluator;
import gv.r5.gateway.domain.LiveMedia;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.domain.Playing;
import gv.r5.gateway.remote.RemoteProxy;
import gv.r5.gateway.repo.LiveMediaRepo;
import gv.r5.gateway.repo.MediaRepo;
import gv.r5.gateway.repo.PlayingRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlayService {

    private Logger log = LoggerFactory.getLogger(PlayService.class);

    @Autowired
    private MediaRepo mediaRepo;

    @Autowired
    private Evaluator evaluator;

    @Autowired
    private PlayingRepo playingRepo;

    @Autowired
    private LiveMediaRepo liveMediaRepo;

    @Autowired
    private RemoteProxy proxy;

    @Transactional
    public Playing start(String key, String frame) {
        Media m = mediaRepo.getByKey(key);
        log.info("find media by key {}, media {}", key, m);
        MediaServer s = evaluator.recorded(m);
        log.info("choose media server {}", s);
        Playing p = s.start(m, frame, proxy);
        log.info("start media result {}", p);
        playingRepo.save(p);
        return p;
    }

    @Transactional
    public Playing live(String addr, Long duration, String frame) {
        LiveMedia lv = liveMediaRepo.create(addr);
        liveMediaRepo.save(lv);
        MediaServer s = evaluator.live(addr);
        log.info("choose media server {}", s);
        Playing p = s.live(lv, duration, frame, proxy);
        log.info("start live result {}", p);
        playingRepo.save(p);
        return p;
    }
}
