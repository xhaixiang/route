package gv.r5.gateway.services;

import gv.r5.gateway.core.Evaluator;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.MediaServer;
import gv.r5.gateway.remote.RemoteProxy;
import gv.r5.gateway.repo.FileStore;
import gv.r5.gateway.repo.MediaRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Service
public class CaptureService {

    private Logger logger = LoggerFactory.getLogger(CaptureService.class);

    @Autowired
    private Evaluator evaluator;

    @Autowired
    MediaRepo mediaRepo;

    @Autowired
    private RemoteProxy client;

    @Autowired
    private FileStore fileStore;

    public byte[] capture(String key, Long captureTime, TimeUnit seconds) throws IOException {
        if (captureTime == null) {
            captureTime = 5 * 60L;
        }
        Media m = mediaRepo.getByKeyIgnoreStatus(key);
        logger.info("find media by key {}, media {}", key, m);
        MediaServer s = evaluator.recorded(m);
        logger.info("choose media server {}", s);
        return fileStore.open(s.capture(m, captureTime, client));
    }
}
