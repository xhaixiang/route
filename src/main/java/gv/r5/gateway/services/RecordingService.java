package gv.r5.gateway.services;

import gv.r5.gateway.core.Evaluator;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.Recording;
import gv.r5.gateway.remote.RemoteProxy;
import gv.r5.gateway.repo.MediaRepo;
import gv.r5.gateway.repo.RecordingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class RecordingService {

    @Autowired
    private MediaRepo mediaRepo;

    @Autowired
    private RecordingRepo recordingRepo;

    @Autowired
    private TimerService timerService;

    @Autowired
    private RemoteProxy proxy;

    @Autowired
    private Evaluator evaluator;

    @Transactional
    public Recording grab(String streamUrl, Date start, Date end) {
        Media m = mediaRepo.createRecordingMedia(streamUrl);
        mediaRepo.save(m);
        Recording r = recordingRepo.create(m, streamUrl, start, end);
        recordingRepo.save(r);
        timerService.add(new RecordingTask(m, r, proxy, evaluator, recordingRepo));
        return r;
    }

    public void finish(String name) {
        Media media = mediaRepo.getByKeyIgnoreStatus(name);
        recordingRepo.finish(media.getId());
    }

    public void start(String name) {
        Media media = mediaRepo.getByKeyIgnoreStatus(name);
        recordingRepo.start(media.getId());
    }
}
