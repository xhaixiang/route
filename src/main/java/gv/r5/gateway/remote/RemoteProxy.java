package gv.r5.gateway.remote;

import org.springframework.stereotype.Component;
import retrofit.RestAdapter;

@Component
public class RemoteProxy {

    public <T> T get(Class<T> clazz, String serverUrl){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL).setEndpoint(serverUrl).build();
        return restAdapter.create(clazz);

    }

}
