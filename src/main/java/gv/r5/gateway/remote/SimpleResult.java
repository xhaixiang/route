package gv.r5.gateway.remote;

/**
 * Created by xuhaixiang on 7/21/14.
 */
public class SimpleResult {

    private String status;

    private String name;

    private String streamUrl;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    @Override
    public String toString() {
        return "SimpleResult{" +
                "status='" + status + '\'' +
                ", name='" + name + '\'' +
                ", streamUrl='" + streamUrl + '\'' +
                '}';
    }

    public boolean isOK() {
        return "SUCCESS".equals(status);
    }
}
