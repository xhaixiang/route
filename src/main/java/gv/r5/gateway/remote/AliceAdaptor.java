package gv.r5.gateway.remote;

import gv.r5.gateway.domain.LiveMedia;
import gv.r5.gateway.domain.Media;
import gv.r5.gateway.domain.Recording;
import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Query;

public interface AliceAdaptor {
    @POST("/alice/start")
    SimpleResult start(@Body Media media, @Query("f") String frame);

    @POST("/alice/live")
    SimpleResult live(@Body LiveMedia media, @Query("t") Long duration, @Query("f") String frame);

    @POST("/alice/capture/media")
    SimpleResult capture(@Body Media media, @Query("t") Long captureTime);

    @POST("/alice/capture/live")
    SimpleResult capture(@Body LiveMedia media, @Query("t") Long duration);

    @POST("/alice/recording")
    SimpleResult recording(@Body Media media, @Query("s") String streamUrl, @Query("t") Long duration);

}
