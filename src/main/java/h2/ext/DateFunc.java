package h2.ext;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFunc {
    public static String mysqlDate(java.sql.Timestamp ts)
    {
        Date d = new Date(ts.getTime());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(d);
    }
}
